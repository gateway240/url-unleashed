import { useEffect, useRef, useState } from 'react'
import { Col, Form, Panel, Row, Schema } from 'rsuite'

import './App.css'
import 'rsuite/dist/rsuite.min.css'

import CheckRoundIcon from '@rsuite/icons/CheckRound'
import FolderFillIcon from '@rsuite/icons/FolderFill'
import PageIcon from '@rsuite/icons/Page'
import SpinnerIcon from '@rsuite/icons/legacy/Spinner'
import WarningRoundIcon from '@rsuite/icons/WarningRound'

import useDebounce from './hooks/useDebounce'
import {
  defaultUrlResponse,
  UrlType,
  useBackendService,
} from './useBackendService'

const model = Schema.Model({
  url: Schema.Types.StringType()
    .isRequired('This field is required.')
    .isURL('Valid URL is required.'),
})

const TextField = (props: any) => {
  const { name, label, accepter, ...rest } = props
  return (
    <Form.Group controlId={`${name}-3`}>
      <Form.ControlLabel>{label} </Form.ControlLabel>
      <Form.Control name={name} accepter={accepter} {...rest} />
    </Form.Group>
  )
}

function App() {
  const formRef = useRef() as any
  const backendService = useBackendService()
  const [loading, setLoading] = useState(false)
  const [urlData, setUrlData] = useState(defaultUrlResponse)
  const [, setFormError] = useState({})
  const [formValue, setFormValue] = useState<Record<string, any>>({
    url: 'https://',
  })
  const debouncedFormValue = useDebounce<Record<string, any>>(formValue, 500)

  useEffect(() => {
    const fetchData = async () => {
      // get the data from the api
      const data = await backendService.validateUrl(debouncedFormValue.url)
      setUrlData(data)
    }

    if (formRef.current?.check()) {
      // Url is Valid
      setLoading(true)
      // call the function
      fetchData()
        // make sure to catch any error
        .catch(console.error)
        .finally(() => setLoading(false))
    } else {
      setUrlData(defaultUrlResponse)
      setLoading(false)
    }
  }, [debouncedFormValue])

  return (
    <>
      <Row className="container">
        <Form
          ref={formRef}
          onChange={setFormValue}
          onCheck={setFormError}
          formValue={formValue}
          model={model}
        >
          <TextField name="url" label="Input Url" />
        </Form>
      </Row>
      <Row className="container">
        <Col className="card">
          <Panel className="panel" shaded header="Exists">
            {loading ? (
              <SpinnerIcon pulse />
            ) : urlData.valid ? (
              <CheckRoundIcon color="green"></CheckRoundIcon>
            ) : (
              <WarningRoundIcon color="red"></WarningRoundIcon>
            )}
          </Panel>
        </Col>
        <Col className="card">
          <Panel className="panel" shaded header="Type">
            {loading ? (
              <SpinnerIcon pulse />
            ) : urlData.type === UrlType.Directory ? (
              <FolderFillIcon></FolderFillIcon>
            ) : (
              <PageIcon></PageIcon>
            )}
          </Panel>
        </Col>
      </Row>
    </>
  )
}

export default App

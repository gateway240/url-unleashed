export type validateUrlResponse = {
  valid: boolean
  type: UrlType
}

export enum UrlType {
  File,
  Directory,
}

export const defaultUrlResponse: validateUrlResponse = {
  valid: false,
  type: UrlType.File,
}

export const useBackendService = () => {
  const validateUrl = async (url: string) => {
    console.log(`Validating URL: ${url}`)
    await new Promise((resolve) => setTimeout(resolve, 1000)) // replace with real backend
    return {
      // mocked response with random boolean
      valid: Math.random() < 0.5,
      type: Math.random() < 0.5 ? UrlType.Directory : UrlType.File,
    }
  }

  return {
    validateUrl,
  }
}
